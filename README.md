# Sanar Challenge
Sanar Challenge API RESTful

## Install
Install dependencies with composer
```shell
composer install
```

## Run Server
Run the following command in terminal to start localhost web server:

```shell
php -S localhost:8888 -t public index.php
```

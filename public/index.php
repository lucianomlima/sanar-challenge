<?php
define('BASE_PATH', __DIR__.'/..');
define('DATA_PATH', __DIR__.'/data');
define('CONFIG_PATH', BASE_PATH.'/config');

require CONFIG_PATH.'/bootstrap.php';
require CONFIG_PATH.'/constants.php';
$settings = require CONFIG_PATH.'/settings.php';

// Instantiate the app
$app = new Slim\App($settings);

// Set up dependencies
require CONFIG_PATH.'/dependencies.php';

// Register middleware
require CONFIG_PATH.'/middlewares.php';

// Register routes
require CONFIG_PATH.'/routes.php';

$app->run();

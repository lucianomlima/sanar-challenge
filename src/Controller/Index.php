<?php
namespace Sanar\Controller;

use Sanar\Model\{
    Ano,
    Assuntos,
    Banca,
    Cargo,
    File,
    Nivel
};

use \Psr\Http\Message\{
    ResponseInterface as Response,
    ServerRequestInterface as Request
};
use \Psr\Log\LoggerInterface;
use \Slim\Container;

/**
 * Index controller
 */
class Index
{
    /**
    * @var Container
    */
    protected $container;

    /**
    * @var LoggerInterface
    */
    protected $logger;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    public function index(Request $request, Response $response, $args)
    {
        $this->_log(__FUNCTION__, $args);
        $data = [
            'author' => 'Luciano Lima',
            'email' => 'lucianomlima@ymail.com',
            'project' => 'System\'s Engineer challenge for Sanar Editora',
        ];

        return $response->withJson($data);
    }

    public function register(Request $request, Response $response, $args)
    {
        $this->_log(__FUNCTION__, $args);

        try {
            $data = $request->getParsedBody();

            if (!is_array($data) OR empty($data)) {
                throw new \Exception('No data sent');
            }

            $filtered = $this->validate($data);

            $this->saveData($filtered);

            return $response->withJson([
                "message" => "Cadastro feito com sucesso"
            ]);
        } catch (\Exception $e) {
            $error = [
                'message' => $e->getMessage()
            ];

            if ($code) {
                $error['code'] = $code;
            }

            return $response->withJson($error);
        }
    }

    protected function validate($args)
    {
        $banca = new Banca($args['banca']);
        if (!$banca->isValid()) {
          throw new \Exception($banca->getErrorMessage());
        }

        $cargo = new Cargo($args['cargo']);
        if (!$cargo->isValid()) {
          throw new \Exception($cargo->getErrorMessage());
        }

        $ano = new Ano($args['ano']);
        if (!$ano->isValid()) {
          throw new \Exception($ano->getErrorMessage());
        }

        $nivel = new Nivel($args['nivel']);
        if (!$nivel->isValid()) {
            throw new \Exception($nivel->getErrorMessage());
        }

        $assuntos = new Assuntos($args['assuntos']);
        if (!$assuntos->isValid()) {
            throw new \Exception($assuntos->getErrorMessage());
        }

        return [
            'banca' => $banca->getData(),
            'cargo' => $cargo->getData(),
            'ano' => $ano->getData(),
            'nivel' => $nivel->getData(),
            'assuntos' => $assuntos->getData()
        ];
    }

    protected function saveData($data)
    {
        $file = new File;
        $file->setData($data);
        $file->save();
    }

    protected function _log($functionName, $args = NULL)
    {
        $calledClass = get_called_class();
        $className = substr(strrchr(rtrim($calledClass, '\\'), '\\'), 1);

        $this->logger->info("{$className}: {$functionName}", $args);
    }
}

<?php
namespace Sanar\Model;

interface ModelInterface
{
  public function isValid();
  public function getData();
  public function getErrorMessage();
}

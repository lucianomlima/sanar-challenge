<?php
namespace Sanar\Model;

/**
 * Assuntos Model
 */
class Assuntos implements ModelInterface
{
  /**
   * @var Array
   */
  private $data;

  /**
  * @var Array
  */
  private $allowed = [
    'portugues',
    'matematica',
    'redacao',
    'administracao',
    'filosofia',
    'medicina',
    'logica',
    'história',
    'farmacia'
  ];

  /**
   * @var String
   */
  private $error;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public function isValid()
  {
    if (empty($this->data)) {
      $this->error = '[Assuntos] Parâmetro não informado';
      return false;
    }

    if (!is_array($this->data)) {
      $this->error = '[Assuntos] Formato não permitido';
      return false;
    }

    $this->data = filter_var($this->data, FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
    $isValid = true;
    foreach ($this->data as $subject) {
      if (!in_array($subject, $this->allowed)) {
        $isValid = false;
        break;
      }
    }

    if (!$isValid) {
      $this->error = '[Assuntos] Parâmetro inválido';
      return false;
    }

    return true;
  }

  public function getData()
  {
    return $this->data;
  }

  public function getErrorMessage()
  {
    return $this->error;
  }
}

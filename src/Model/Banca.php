<?php
namespace Sanar\Model;

/**
 * Banca Model
 */
class Banca implements ModelInterface
{
  /**
   * @var String
   */
  private $data;

  /**
   * @var Int
   */
  private $maxLength = 250;

  /**
   * @var String
   */
  private $error;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public function isValid()
  {
    if (empty($this->data)) {
      $this->error = '[Banca] Parâmetro não informado';
      return false;
    }

    if (!is_string($this->data)) {
      $this->error = '[Banca] Parâmetro inválido';
      return false;
    }

    $this->data = filter_var($this->data, FILTER_SANITIZE_STRING);
    if (strlen($this->data) > $this->maxLength) {
      $this->error = '[Banca] Número de caracteres excedidos: 250';
      return false;
    }

    return true;
  }

  public function getData()
  {
    return $this->data;
  }

  public function getErrorMessage()
  {
    return $this->error;
  }
}

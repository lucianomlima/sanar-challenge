<?php
namespace Sanar\Model;

/**
 * File Model
 */
class File
{
  /**
   * @var Array
   */
  private $data;

  /**
   * @var Resource
   */
  private $file;

  /**
   * @var String
   */
  private $filepath = DATA_PATH.'/arquivo.txt';

  /**
   * @var String
   */
  private $content;

  public function __construct()
  {
    $this->checkDir();
    $this->checkFile();
  }

  public function setData($data)
  {
    $this->data = $data;
  }

  public function save()
  {
    if (!empty($this->content)) {
      $this->content = $this->mergeData();
    } else {
      $this->content = $this->encode([$this->data]);
    }

    fwrite($this->file, $this->content);
    fclose($this->file);
  }

  private function checkDir()
  {
    if (!file_exists(DATA_PATH) OR !is_dir(DATA_PATH)) {
        mkdir(DATA_PATH, 0755);
    }
  }

  private function checkFile()
  {
    if (!file_exists($this->filepath) OR !is_file($this->filepath)) {
      $this->content = '';
    } else {
      $this->content = file_get_contents($this->filepath);
    }

    $this->file = fopen($this->filepath, 'w+');
  }

  private function mergeData()
  {
    $data = $this->decode($this->content);
    array_push($data, (object)$this->data);
    return $this->encode($data);
  }

  private function decode($data)
  {
    return json_decode($data);
  }

  private function encode($data)
  {
    return json_encode($data);
  }
}

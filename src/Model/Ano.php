<?php
namespace Sanar\Model;

/**
 * Ano Model
 */
class Ano implements ModelInterface
{
  /**
   * @var Int
   */
  private $data;

  /**
   * @var Int
   */
  private $length = 4;

  /**
   * @var String
   */
  private $error;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public function isValid()
  {
    if (empty($this->data)) {
      $this->error = '[Ano] Parâmetro não informado';
      return false;
    }

    if (!is_numeric($this->data)) {
      $this->error = '[Ano] Formato não permitido';
      return false;
    }

    $this->data = filter_var($this->data, FILTER_SANITIZE_NUMBER_INT);
    if (strlen($this->data) != $this->length) {
      $this->error = '[Ano] Parâmetro inválido';
      return false;
    }

    return true;
  }

  public function getData()
  {
    return $this->data;
  }

  public function getErrorMessage()
  {
    return $this->error;
  }
}

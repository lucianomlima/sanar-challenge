<?php
namespace Sanar\Model;

/**
 * Cargo Model
 */
class Cargo implements ModelInterface
{
  /**
   * @var String
   */
  private $data;

  /**
  * @var Array
  */
  private $allowed = [
    'Psicologo',
    'Dentista',
    'Nutricionista',
    'Medico',
    'Fisioterapeuta',
    'Medico Veterinario',
    'Enfermeiro',
    'Farmaceutico'
  ];

  /**
   * @var String
   */
  private $error;

  public function __construct($data)
  {
    $this->data = $data;
  }

  public function isValid()
  {
    if (empty($this->data)) {
      $this->error = '[Cargo] Parâmetro não informado';
      return false;
    }

    if (!is_string($this->data)) {
      $this->error = '[Cargo] Formato não permitido';
      return false;
    }

    $this->data = filter_var($this->data, FILTER_SANITIZE_STRING);
    if (!in_array($this->data, $this->allowed)) {
      $this->error = '[Cargo] Parâmetro inválido';
      return false;
    }

    return true;
  }

  public function getData()
  {
    return $this->data;
  }

  public function getErrorMessage()
  {
    return $this->error;
  }
}

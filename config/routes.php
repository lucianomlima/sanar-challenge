<?php
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

// Routes
$app->get('/',  Sanar\Controller\Index::class.':index');
$app->post('/register', Sanar\Controller\Index::class.':register');

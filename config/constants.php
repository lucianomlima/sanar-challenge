<?php
// Time
define('ONE_MINUTE', 60);
define('ONE_HOUR', ONE_MINUTE * 60);
define('ONE_DAY', ONE_HOUR * 24);
define('ONE_WEEK', ONE_DAY * 7);
define('ONE_MONTH', ONE_DAY * 30);
define('ONE_YEAR', ONE_DAY * 365);

<?php
use Slim\HttpCache\CacheProvider;

$container = $app->getContainer();

// error handlers
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $c['response']
            ->withStatus(404)
            ->withJson([
                'status' => 'error',
                'message' => 'Resource not found',
            ]);
    };
};
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withJson([
                'status' => 'error',
                'message' => 'Method not allowed',
                'data' => [
                    'methods' => 'Allowed methods: ' . implode(', ', $methods)
                ],
            ]);
    };
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], Monolog\Logger::DEBUG));
    return $logger;
};

// cache
$container['cache'] = function () {
    return new CacheProvider();
};

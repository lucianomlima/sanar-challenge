<?php
ini_set('date.timezone', 'America/Bahia');
$date = new Datetime;

return [
    'settings' => [
        'addContentLengthHeader' => false,
        'displayErrorDetails' => true,
        'logger' => [
            'name' => 'API',
            'path' => __DIR__.'/../logs/app_'.$date->format('Y-m-d').'.log',
        ],
    ],
];
